<?php namespace Mambo\CMS;

use \Illuminate\Support\ServiceProvider;

class CMSServiceProvider extends ServiceProvider {

  public function register()
  {
    // Bind main App object
    $this->app->bindShared('mambo_cms', function() {
      return new CMS('CMamboS Demo');
    });

    // Merge config file
    $this->mergeConfigFrom(
      __DIR__.'/Config/cms.php', 'cms'
    );
  }

  public function boot()
  {
    // Publish config file
    $this->publishes([
      __DIR__.'/Config/cms.php' => config_path('cms.php')
    ]);

    // Load views
    $this->loadViewsFrom(__DIR__.'/Resources/Views', 'cms');

    // Add custom routes
    include __DIR__.'/routes.php';
  }

}
