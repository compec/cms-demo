<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Nombre de la aplicación
  |--------------------------------------------------------------------------
  |
  | Nombre de la aplicación usado en el dashboard del CMS.
  |
  */

  'name' => 'CMamboS Demo',

  /*
  |--------------------------------------------------------------------------
  | Prefijo del CMS
  |--------------------------------------------------------------------------
  |
  | URL de entrada al CMS.
  |
  */
  
  'url_prefix' => 'admin'

];
