<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>{{ $appName }}</title>
  <style type="text/css">
    body {
      color: #444;
      font-size: 14px;
      padding-top: 50px;
      background-color: #efefef;
      font-family: arial, sans-serif;
    }
    .container {
      margin: 0 auto;
      max-width: 600px;
    }
    .hint {
      color: #888;
    }
  </style>
</head>
<body>

  <div class="container">
    <h1>{{ $appName }}</h1>

    <p>
      Bienvenid@,
    </p>
    <p>
      Esta es una nueva instalación del CMS Mambo, disfruta el viaje.<br>
      <span class="hint">- @javichito</span>
    </p>
  </div>

</body>
</html>
