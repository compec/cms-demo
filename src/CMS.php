<?php namespace Mambo\CMS;

class CMS {

  protected $name;

  public function __construct($name)
  {
    $this->name = $name;
  }

  public function getName()
  {
    return $this->name;
  }

}
