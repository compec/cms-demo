<?php

$prefix = Config::get('cms.url_prefix');

Route::group(['prefix' => $prefix], function() {

  Route::get('/', function() {
    $appName = Config::get('cms.name');

    return view('cms::hello', [
      'appName' => $appName
    ]);
  });

});
