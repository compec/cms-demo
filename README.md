# CMamboS demo

## Instalación

Para usar el CMS en tu proyecto de Laravel 5 sigue los siguientes pasos:

1. Agrega `"mambo/cms": "dev-master"` en `"require"` de tu `composer.json`.
2. Agrega lo siguiente a `composer.json`:
    
        "repositories": [
          {
            "type": "vcs",
            "url": "https://bitbucket.org/compec/cms-demo"
          }
        ]

3. Ejecuta `composer update`.
4. Agrega `'Mambo\CMS\CMSServiceProvider'` al arreglo `"providers"` del archivo `config/app.php`.
5. Ejecuta `php artisan vendor:publish` para publicar los archivos de configuración/migraciones/seeders a tu proyecto actual.
6. Profit!.
